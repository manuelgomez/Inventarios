﻿using System;

namespace Inventarios.Models
{
    public class User
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public int Profile { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public DateTime Created { get; set; }
        public string IPAuthorized { get; set; }

        public string Token { get; internal set; }
        public DateTime Generated { get; internal set; }

        public string Error { get; set; }
        
    }
}
