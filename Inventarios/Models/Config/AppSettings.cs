﻿namespace Inventarios.Models.Config
{
    public class AppSettings
    {
        public string Url { get; set; }
        public string Secret { get; set; }
    }
}
