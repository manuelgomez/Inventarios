﻿namespace Inventarios.Models.Config
{
    public class DataBaseSettings
    {
        public string Provider { get; set; }
        public string Connection { get; set; }
        public string Credentials { get; set; }
    }
}
