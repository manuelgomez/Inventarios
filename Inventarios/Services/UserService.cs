﻿using Inventarios.Models;
using Inventarios.Models.Config;
using Inventarios.Repositories;
using Inventarios.ViewModels;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NETCore.MailKit.Core;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Inventarios.Services
{
    public interface IUserService
    {
        Task<User> AuthenticateAsync(LoginViewModel login);

    }

    public class UserService : IUserService
    {
        readonly AppSettings _app;
        readonly IEmailService _mail;

        public UserService(IOptions<AppSettings> app, IEmailService mail)
        {
            _app = app.Value;
            _mail = mail;
        }

        public async Task<User> AuthenticateAsync(LoginViewModel login)
        {
            Log.Logger.Information("Logging process");
            var user = await LoadUserAsync(login.Login);
            if (user == null) return new User { Id = -1, Error = "Error con el Usario" };

            var isPasswordOk = user.Password == login.Password;
            if (!isPasswordOk) return new User { Id = -1, Error = "Error con el Usario/Clave" };

            var key = Encoding.ASCII.GetBytes(_app.Secret);
            var tokenHandler = new JwtSecurityTokenHandler();
            var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Sid, user.Id.ToString()), new Claim(ClaimTypes.Name, user.Name) });
            //var claims = user.Roles.Select(r => new Claim(ClaimTypes.Role, r.Name));
            //identity.AddClaim(claims);

            var tokenDescription = new SecurityTokenDescriptor {
                Subject = identity,
                Expires = DateTime.UtcNow.AddDays(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescription);
            user.Token = tokenHandler.WriteToken(token);
            user.Generated = DateTime.UtcNow;
            user.Password = "";

            return user;
        }

        private static async Task<User> LoadUserAsync(string login)
        {
            using var repo = new UserRepository();
            return await repo.GetByLogin(login);
        }
    }
}
