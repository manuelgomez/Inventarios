﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Inventarios.Infrastructure
{
    sealed class DataBase : IDisposable
    {
        const string Secret = "#@Protekto2022**";

        static DbProviderFactory _factory;
        static string _connection;

        public IDbConnection Context { get; private set; }

        public DataBase()
        {
            if (_factory == null) throw new ApplicationException("Factory not defined!!");

            Context = _factory.CreateConnection() ?? throw new DataException("Cant connect to Database");
            Context.ConnectionString = _connection;

            if (Context == null) throw new ApplicationException("Cant create DB context");
        }

        public void Dispose()
        {
            Context?.Dispose();
            Context = null;
            GC.SuppressFinalize(this);
        }

        ~DataBase() { Context?.Dispose(); }

        public static void RegisterFactory()
        {
            DbProviderFactories.RegisterFactory("System.Data.SqlClient", SqlClientFactory.Instance);
        }

        public static void SetConnection(string provider, string connection, string credentials = "")
        {
            if (string.IsNullOrWhiteSpace(provider)) throw new ArgumentException("Provider is required");
            if (string.IsNullOrWhiteSpace(connection)) throw new ArgumentException("Connection is required");

            //var isClear = credentials.IndexOf("User Id", StringComparison.InvariantCultureIgnoreCase) > -1;
            _factory = DbProviderFactories.GetFactory(provider);
            _connection = $"{connection}{credentials}";
        }
    }
}
