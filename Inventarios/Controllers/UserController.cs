﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Inventarios.Services;
using Inventarios.ViewModels;
using Inventarios.Models;

namespace Inventarios.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        readonly IUserService _user;

        public UserController(IUserService user)
        {
            _user = user;
        }

        [AllowAnonymous][HttpPost("Authenticate")]
        public async Task<ActionResult<User>> AuthenticateAsync(LoginViewModel login)
        {
            var user = await _user.AuthenticateAsync(login);

            if (user.Id == -1)
                return BadRequest(new { message = user.Error });

            return Ok(user);
        }


    }
}
