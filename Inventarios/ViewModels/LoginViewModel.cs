﻿using System.ComponentModel.DataAnnotations;

namespace Inventarios.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "El usuario es obligatorio")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Clave obligatoria")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
