﻿using Insight.Database;
using Inventarios.Infrastructure;
using Inventarios.Models;
using Serilog;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Inventarios.Repositories
{
    sealed class UserRepository : IDisposable
    {
        DataBase _db;

        public UserRepository()
        {
            _db = new DataBase();
        }

        public void Dispose()
        {
            _db?.Dispose();
            _db = null;

            GC.SuppressFinalize(this);
        }

        ~UserRepository()
        {
            _db?.Dispose();
        }

        const string Query =
            @"SELECT ua.ua_id [Id], ua.co_id [ContactId], aua.aua_perfil [Profile], ua.ua_usr [Login], ua.ua_con [Password], 
                     co.co_nombre + ' ' + co.co_apellido [Name], co.co_correo [Mail], co.co_direccion [Address], co.co_telcasa [Phone], co.co_telcelular [Cell], 
                     co.co_fechains [Created], IsNull(uaip.ip, 'NA') IPAutorized
                FROM [dbo].[usuarioaplicacion] ua 
                JOIN [dbo].[accesousuarioaplicacion] aua On aua.ua_id = ua.ua_id
                LEFT JOIN [dbo].[usuarioaccesoip] uaip On ua.ua_id = uaip.ua_id AND uaip.uaip_fechafin IS NULL
                LEFT JOIN [dbo].[contacto] co On co.co_id = ua.co_id ";


        const string ByUserName = "WHERE ua.ua_usr=@userName ";

        public async Task<User> GetByLogin(string login)
        {
            try {
                return await _db.Context.SingleSqlAsync<User>(Query + ByUserName, new { login });
            }
            catch(SqlException sqle) {
                Log.Error(sqle, "UserRepository.GetByName {login}", login);
            }
            catch(Exception ex) {
                Log.Error(ex, "UserRepository.GetByName {login}", login);
            }
            return null;
        }
    }
}
