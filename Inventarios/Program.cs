using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;

namespace Inventarios
{
    public class Program
    {
        public static void Main(string[] args) {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;
            AppDomain.CurrentDomain.FirstChanceException += FirstChanceException;
            AppDomain.CurrentDomain.ProcessExit += ProcessExit;

            try {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex) {
                EmergencyLogger();
                Log.Logger.Fatal(ex, "Unexpected termination");
            }
            finally {
                Log.CloseAndFlush();

                AppDomain.CurrentDomain.ProcessExit -= ProcessExit;
                AppDomain.CurrentDomain.FirstChanceException -= FirstChanceException;
                AppDomain.CurrentDomain.UnhandledException -= UnhandledException;
            }
        }

        private static void ProcessExit(object sender, EventArgs e) {
            EmergencyLogger();
            Log.Logger.Information("Application is shutting down... from:{source}\n{e}", sender, e);
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                //.ConfigureLogging((context, config) => config.ClearProviders())
                .UseSerilog((context,config) => config
                    .ReadFrom.Configuration(context.Configuration)
                    .Enrich.FromLogContext()
                    .Enrich.WithProperty("Application", typeof(Program).Assembly.GetName().Name)
                    .Enrich.WithProperty("Enviroment", context.HostingEnvironment)
                )
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                });

        static void EmergencyLogger() {
            if (Log.Logger == null || Log.Logger.GetType().Name == "SilentLogger")
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.Console()
                    .CreateLogger();
        }

        static void FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e) {
            EmergencyLogger();
            Log.Logger.Error(e.Exception, "First change exception: {source}\n{exception}", e.Exception.Source, e.Exception);
        }

        static void UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            var ex = e.ExceptionObject as Exception;
            EmergencyLogger();

            if (e.IsTerminating)
                Log.Fatal(ex, "Unhandled Fatal Exception");
            else
                Log.Error(ex, "Unhandled Exception");
        }
    }
}
